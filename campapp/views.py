from django.shortcuts import render
#from campapp.models import contact,student
from django.http import HttpResponse,HttpResponseRedirect
from campapp.models import company,postj,addQuestion,apply1,student,contact
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
import datetime
from django.contrib.auth import authenticate, login,logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import check_password
def index(request):
    return HttpResponse('welcome to my page')
def dash(request):
     return render(request,'dashcomp.html')


     
@login_required
def quizresult(request):
     qu=addQuestion.objects.filter(name__user__username=request.user.username)
     print(qu)
     return render(request,'quizresult.html',{'all':qu})
@login_required
def jobs(request):
     all_jobs = postj.objects.filter(name__user__username=request.user.username)
     print(all_jobs)
     return render(request,'alljobs.html',{'all':all_jobs})

@login_required
def job(request):
     all_jobs = postj.objects.all().order_by('-id')
     if request.method=='POST':
          sj=request.POST['jb']
          all_jobs = postj.objects.filter(position__contains=sj)
          return render(request,'alljob.html',{'all':all_jobs,'total':len(all_jobs)})
     return render(request,'alljob.html',{'all':all_jobs,'total':len(all_jobs)})

     
@login_required
def latestcomp(request):
     comp = company.objects.all().order_by('-id')[:6]
     print(comp)
     return render(request,'latestcomp.html',{'all':comp})

def postjob(request):
     return render(request,'postjob.html')
@login_required
def chngpass(request):
     user_obj=User.objects.get(username=request.user.username)
     
     if request.method=="POST":
          oldp=request.POST['old']
          newpass=request.POST['newp']
          match=check_password(oldp,user_obj.password)
          if match==True:
               user_obj.set_password(newpass)
               user_obj.save()
               return render(request,'chngpass.html',{'status':'password change successfully!!!'})
     return render(request,'chngpass.html')

@login_required
def chngpass1(request):
     user_obj=User.objects.get(username=request.user.username)
     
     if request.method=="POST":
          oldp=request.POST['old']
          newpass=request.POST['newp']
          match=check_password(oldp,user_obj.password)
          if match==True:
               user_obj.set_password(newpass)
               user_obj.save()
               return render(request,'chngpass1.html',{'status':'password change successfully!!!'})
     return render(request,'chngpass1.html')

@login_required
def editpr(request):
     comp = company.objects.get(user__username=request.user.username)
     if request.method=='POST':
          el=request.POST['em']
          abt=request.POST['abt']
          cp=request.POST['contp']
          serv=request.POST['ser']
          yer=request.POST['year']
          hd=request.POST['head']
          cy=request.POST['cit']
          ct=request.POST['cont']
          adr=request.POST['addr']

          comp.email=el
          comp.about=abt
          comp.contactperson=cp
          comp.services=serv
          comp.year=yer
          comp.headoffice=hd
          comp.city=ct
          comp.Address=adr


          # data=company(email=el,about=abt,contactperson=cp,services=serv,year=yer,headoffice=hd,city=cy,contact=ct,Address=adr)
          comp.save()
     
          return render(request,'editprofile.html',{'status':'Changes saved Successfully!!!'})
     return render(request,'editprofile.html')
@login_required
def editstu(request):
     std=student.objects.get(user__username=request.user.username)
     if request.method=='POST':
          fnm=request.POST['fn']
          lnm=request.POST['ln']
          email=request.POST['em']
          dat=request.POST['dob']
          gndr=request.POST['gn']
          myc=request.POST['mc']
          mycntr=request.POST['mcon']
          stimg=request.POST['sim']
          std.user__email=email
          std.user__first_name=fnm
          std.user__first_name=lnm
          std.user.save()

          std.City=myc
          std.Date_Of_Birth=dat
          std.Gender=gndr
          std.country=mycntr
          std.profile_pic=stimg
          std.save()
          return render(request,'edit.html',{'status':'Changes saved Successfully!!!'})
     return render(request,'edit.html')

@login_required
def edit(request):
     comp = company.objects.get(user__username=request.user.username)
     if request.method=='POST':
          abo=request.POST['ab']
          mn=request.POST['mai']
          cp=request.POST['conti']
          ei=request.POST['eml']
          yr=request.POST['ye']
          compan=request.POST['cp']
          serv=request.POST['se']
          wb=request.POST['we']

          comp.user__email=ei
          comp.user__first_name=compan
          comp.user.save()
          comp.Web=wb
          comp.about=abo
          comp.services=serv
          comp.year=yr
          comp.headoffice=mn
          comp.contactperson=cp
          comp.save()
          # data=company(email=el,about=abt,contactperson=cp,services=serv,year=yer,headoffice=hd,city=cy,contact=ct,Address=adr)
          #comp.save()
     
          return render(request,'edit1.html',{'status':'Changes saved Successfully!!!'})
     return render(request,'edit1.html',{'profile':comp})

def latestvac(request):
     return render(request,'latestvac.html')

def applyvac(request):
     all=apply1.objects.filter(Student__user__username=request.user.username)
   
     # id=request.GET['id']
     # jobdes= postj.objects.get(id=id)
     # jobid = jobdes.id
     #rqs= applyvac.services.split(',')
     return render(request,'vacancy.html',{'all':all})


def applicants(request):
     
     all=apply1.objects.filter(cmpanyid__user__username=request.user.username)
     return render(request,'applicants.html',{'all':all})
     
def about(request):
     return render(request,'about.html')


def contactView(request):
     if request.method=='POST':
          f=request.POST['first']
          l=request.POST['last']
          e=request.POST['emai']
          p=request.POST['phone']
          m=request.POST['msg']

          data=contact(first=f,last=l,email=e,phone=p,msg=m)
          data.save()
          msz="Thanks for your feedback!!!"
          return render(request,'contact.html',{'status':msz})
     return render(request,'contact.html')


def uslogin(request):
     if request.method=="POST":
          un = request.POST['username']
          pas = request.POST['password']

          user = authenticate(username=un,password=pas)
          if user:
               if user.is_staff:
                    login(request,user)
                    response=HttpResponseRedirect('/dashcom')
                   
                    response.set_cookie('username',user.username)
                    response.set_cookie('id',user.id)
                    response.set_cookie('logintime',datetime.datetime.now())
                    return response
               elif user.is_active:
                    login(request,user)
                    response=HttpResponseRedirect('/dashstu')
                    if 'rememberme' in request.POST:
                         response.set_cookie('username',user.username)
                         response.set_cookie('id',user.id)
                         response.set_cookie('logintime',datetime.datetime.now())
                    return response
               else:
                    msz = "Your account is not activated"
                    return render(request,'login.html',{'status':msz})
          else:
               msz = "You are not registred user"
               return render(request,'login.html',{'status':msz})
     return render(request,'login.html')

@login_required
def uslogout(request):
     logout(request)
     response=HttpResponseRedirect('/')
     response.delete_cookie('username')
     return response
def signup(request):
     if request.method == "POST":
          if "companysignup" in request.POST:
               name = request.POST['compn']
               email = request.POST['em']
               pas = request.POST['passw']
               ct = request.POST['cit']
               addr = request.POST['addre']
               cont = request.POST['cont']

               user = User.objects.create_user(email,email,pas)
               user.first_name = name
               user.is_staff=True
               user.save()

               comp = company(user=user,Contact_No=cont,city=ct,Address=addr)
               comp.save()
               msz = "Dear {} Thanks for registering with Us".format(name)
               return render(request,'signup.html',{'status':msz})

          elif "studentsignup" in request.POST:
               first = request.POST['first']
               last = request.POST['last']
               email = request.POST['emai']
               pas = request.POST['paswd']
              
               city = request.POST['city']
               addr = request.POST['addr']
               cont = request.POST['con']
               gend=request.POST['gen']
               dat=request.POST['da']
               
               user = User.objects.create_user(email,email,pas)
               user.first_name = first
               user.last_name = last
               
               user.save()

               stu = student(user=user,Contact_No=cont,City=city,Address=addr,Gender=gend,Date_Of_Birth=dat)
               stu.save()
               msz = "Dear {} Thanks for registering with Us".format(first+' '+last)
               return render(request,'signup.html',{'status':msz})
               
     return render(request,'signup.html')

def campus(request):
     all_jobs = postj.objects.all()[:6]
     if request.method=='POST':
          sj=request.POST['jb']
          all_jobs = postj.objects.filter(position__contains=sj)
          t=''
          if len(all_jobs)==0:
               t='No results for "{}"'.format(sj)
          else:
               t='{} total results for "{}"'.format(len(all_jobs),sj)
               
          # msz='Job found successfully!!!'
          return render(request,'campus.html',{'all':all_jobs,'total':t})

     
     if 'username' in request.COOKIES:
          un=request.COOKIES['username']
          user_obj=User.objects.get(username=un)
          login(request,user_obj)
          if user_obj.is_staff:
               return HttpResponseRedirect('/dashcom')
          else:
               return HttpResponseRedirect('/dashstu')
     return render(request,'campus.html',{'all':all_jobs})

def home(request):
     return render(request,'campus.html')

@login_required
def compdes(request):
     if request.method=='GET':
          id=request.GET['id']
          compdes= company.objects.get(id=id)
          tech=''
          if compdes.services != None:
               tech= compdes.services.split(',')
     return render(request,'minimal.html',{'compdescription':compdes,'tech':tech})
@login_required
def jobdes(request):
     qz=addQuestion.objects.all()
     id=request.GET['id']
     jobdes= postj.objects.get(id=id)
     # quiz=addQuestion.objects.all().order_by('id')

     jobid = jobdes.id
     cmpanyid=jobdes.name.id
     no_of_q = len(qz)
     right=0
     wrong=0

     rqs= jobdes.requirements.split(',')
     if request.method=="POST":
          for ques in qz:
               if(request.POST['q'+repr(ques.id)]==request.POST['a'+repr(ques.id)]):
                    right+=1
               else:
                    wrong+=1
          total_score=no_of_q*2
          secured=right*2
          percentage=(secured/total_score)*100
          st=''
          if percentage>=70:
               st='Congratulations you cracked it!!'
               img='/static/images/happy.jpg'
               
               return render(request,'quizresult.html',{'c':cmpanyid,'img':img,'st':st,'tq':no_of_q,'total':total_score,'secured':secured,'percentage':percentage,'rs':'Pass','ap':'ap','p':jobid})
          else:

               img='/static/images/sad1.jpg'
               st='Sorry!! You are not eligible for this job'
               return render(request,'quizresult.html',{'img':img,'st':st,'tq':no_of_q,'total':total_score,'secured':secured,'percentage':percentage,'rs':'Fail','p':jobid})

     return render(request,'android.html',{'jobdescription':jobdes,'rqs':rqs,'que':qz})

def anchor(request):
     return render(request,'anchordesign.html')
#def netmang(request):
     #return render(request,'netmang.html')
def intelli(request):
     return render(request,'intelli.html')
def graphics(request):
     return render(request,'graphicdesign.html')
def dbase(request):
     return render(request,'dbaseh.html')
def mlearn(request):
     return render(request,'dbaseh.html')
def civil(request):
     return render(request,'civile.html')
def hom(request):
     return render(request,'dashcomp.html')
     
def digim(request):
     return render(request,'dbaseh.html')

def netmang(request):
     return render(request,'netmang.html')

def editp(request):
     return render(request,'edit.html')
def passw(request):
     return render(request,'chngpass1.html')


@login_required
def dash(request):
     pp = company.objects.get(user__username=request.user.username)
     all=company.objects.filter(user__username=request.user.username)
     
     cp=student.objects.all()[:5]
     
     return render(request,'dashboardcmp.html',{'profile':pp,'all':all,'cp':cp})

     
@login_required
def dashstu(request):

     pp = student.objects.get(user__username=request.user.username)
     all=student.objects.filter(user__username=request.user.username)
     
     cp=company.objects.all()[:5]
     pj=postj.objects.all()[:4]
     return render(request,'dashboard.html',{'profile':pp,'all':all,'cp':cp,'pj':pj})

@login_required
def postjob(request):
     log = company.objects.get(user__username=request.user.username)
     if request.method=='POST':
          p=request.POST['pos']
          d=request.POST['des']
          h=request.POST['hr']
          r=request.POST['req']
          s=request.POST['sal']
          #da=request.POST['date']
          c=request.POST['cit']
          co=request.POST['con']
          ex=request.POST['exp']
      
          
          data=postj(name=log,position=p,description=d,hrname=h,requirements=r,salary=s,city=c,contact=co,experience=ex)
          data.save()
          if 'im' in request.FILES:
               img = request.FILES['im']
               data.save()
          msz = "job uploaded successfully!!!"
          return render(request,'postjob.html',{'status':msz})

     return render(request,'postjob.html')
     
@login_required
def seeresult(request):
     sd=student.objects.get(user__username=request.user.username)
     jobid = request.GET['id']
     cmpid = request.GET['cmid']
     pj=postj.objects.get(id=jobid)
     cp=company.objects.get(id=cmpid)
     if request.method=='POST':
          ql=request.POST['qua']
          pr=request.POST['per']
          pyr=request.POST['poy']
          rm=request.FILES['re']
          epr=request.POST['exp']

          data=apply1(Student=sd,Job=pj,cmpanyid=cp,Qualification=ql,Graduation_Percentage=pr,Passing_Out_Year=pyr,Resume=rm,Experience=epr)
          data.save()
          if 're' in request.FILES:
               img=request.FILES['re']
               data.save()
          to=request.user.email
          sub='Registeration Validation to Campus Recruitment'
          msg='Thank you,you have successfully registered  to our website!!'
          e=EmailMessage(sub,msg,to=[to,])
          e.send()


          msz='Applied Successfully!!'
          return render(request,'seeresult.html',{"status":msz})
     return render(request,'seeresult.html')

# def forgot(request):
#      if request.method == "POST":
#           if 'forg' in request.POST:
#             username = request.POST['cuser']
            
#             password = request.POST['passw']
#             cpass = request.POST['cpass']
#           if password != cpass:
#                     result = 'Password Does Not Match'
#                     return render(request, 'forgot.html',{'result':result})
#           else:
#                     check = User.objects.filter(username__iexact = username)
#                     result = ''
#           if not check:
#                     result = 'Sorry we could not found you'
#                     return render(request, 'forgot.html',{'result':result})

#           else:
#                     user =''
#                     for i in check:
#                         user = i.username
#                         first = i.first_name
#                         get = User.objects.get(username__iexact=user)
#                         get.set_password(password)
#                         get.save()
#                         result = 'password changed successfully'
#                     return render(request, 'forgot.html',{'un':user, 'f':first,'check':check,'status':result})

#      return render(request, 'forgot.html')

def forgot(request):
    if request.method=='POST':
        name=request.POST['usname']
        pwd=request.POST['password']
        user_obj=User.objects.get(username=name)
        user_obj.set_password(pwd)
        user_obj.save()
        msz='password changed successfully!!!'
        return render(request,'forgot.html',{'status':msz})

    return render(request,'forgot.html')

import random
def finduser(request):
    if request.method=='GET':
        un=request.GET['usern']
        ch=User.objects.filter(username=un)
    if len(ch)==0:
        return HttpResponse('not')
    else:
        user=User.objects.get(username=un)
        to=user.email
        sub='password retrieved'
        rn=random.randint(10000,99999)
        msg='your otp to setup new password is' +str(rn)
        e=EmailMessage(sub,msg,to=[to,])
        e.send()
        return HttpResponse(user.email+','+str(rn))

def checkuser(request):
    if request.method=='GET':
        un=request.GET['usern']
        ch=User.objects.filter(username=un)
        if len(ch)>=1:
            return HttpResponse('user with this name already exists')
        else:
            return HttpResponse('username validation success')
        print(ch,len(ch))

def civil(request):
     return render(request,'civile.html')
def grap(request):
     return render(request,'graphicdesign.html')
def pyth(request):
     return render(request,'pydev.html')
def andr(request):
     return render(request,'android.html')
def net(request):
     return render(request,'netmang.html')
def dbas(request):
     return render(request,'dbaseh.html')

@login_required
def edit_post(request):
    rec = company.objects.get(user__username=request.user.username)
    jobid=request.GET['id']
    
    if 'id' in request.GET:
          id_to_edit=request.GET['id']
          p = postj.objects.get(id=id_to_edit)

          if request.method=='POST':
               pos=request.POST['position']
               sal=request.POST['sal']
               
               reqs=request.POST['req']
               hrname=request.POST['hrname']
               hrcon=request.POST['con']
               city=request.POST['city']
               exp=request.POST['exp']
               des=request.POST['des']
               
               p.position=pos
               p.salary=sal
               p.requirements=reqs
               p.hrname=hrname
               p.contact=hrcon
               p.city=city
               p.experience=exp
               p.description=des
               p.save()

               return render(request,'edit_post.html',{'post':p,'status':'Edited Successfully!!'})
        
    return render(request,'edit_post.html',{'post':p})



@login_required
def delete_post(request):
    rec = company.objects.get(user__username=request.user.username)
    jobid=request.GET['id']
    if 'id' in request.GET:
          id_to_delete=request.GET['id']
          p = postj.objects.get(id=id_to_delete,name=rec)
          p.delete()
    return HttpResponseRedirect('/alljobs')

def check_user(request):
    un = request.GET['usn']
    check = User.objects.filter(username=un)
    if len(check)!=0:
        return HttpResponse(" Exists")
    else:
        return HttpResponse("Username Validation Success!!!")

def sendmail(request):
    if request.method=="POST":
        t=request.POST['to'].split(' ,')
        print(t)
        sb=request.POST['subject']
        msz=request.POST['message']
        msz_obj=EmailMessage(sb,msz,to=t)
        #msz_obj=EmailMessage(sb,msz,to=[t,])
        msz_obj.send()
        return render(request,'sendmail.html',{'status': 'Email sent successfully!!'})        
    return render(request,'sendmail.html')

     








# Create your views here.
